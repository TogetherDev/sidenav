# Side Nav #

#Versions

* **1.1**
* 1.0

## What is it?

This is a utility library for facilitate the creation of side nav. It's builded with the CSS pre-processor [LESS](http://www.lesscss.org/) and the [JQuery](https://jquery.com/).

## Using Side Nav

You can download it and later add to your project.

## Use example:

Import the SideNav.less file to your styles.less file, put the SideNav.js file in your main.js file and in your html page creates a button for open the side nav and an list that represents the side nav.

### Html page

```
#!html

<!DOCTYPE html>
<html>
    <head>
        <title>Side Nav Example</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="resources/css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <span class="fixedOnTopLeft" sideNav="#mainSidenav"></span>
        <ul id="mainSidenav" class="fixedOnTopLeft sideNav autoClose">
            <li class="closeSideNav"><a href="#"></a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a class="disableAutoClose" href="#">Clients</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="resources/js/SideNav.js" type="text/javascript"></script>
        <script src="resources/js/main.js" type="text/javascript"></script>
    </body>
</html>

```

### styles.less

```
#!less

//SideNav.less
@import "https://bitbucket.org/TogetherDev/sidenav/raw/aae72908acf923f79a4f79b244f632e73b1b87b3/less/SideNav.less";

.openSideNav();
.sidenav(@width: 50%);

```

### main.js

```
#!javascript

$(function () {
    prepareSideNavs();
});
```

*Note*: In this example the style.less file was compiled in the server side and stored in the "resources/css/" directory with the "styles.css" name.

## Licensing

**Side Nav** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.