/*
 Copyright 2017 Thomás Sousa Silva.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

function prepareSideNavs(selector = "[sideNav]") {
    $(selector).each(function () {
        var button = $(this);
        var sideNav = $(button.attr("sideNav"));
        var closeSideNav = function (event) {
            if (event !== undefined) {
                var href = $(this).attr("href");
                if ((href === undefined) || (href === "#")) {
                    event.preventDefault();
                }
            }
            sideNav.enableAutoClose = false;
            sideNav.removeClass("opened");
            button.show();
        };
        var openSideNav = function (event) {
            event.preventDefault();
            sideNav.addClass("opened");
            button.hide();
            if (sideNav.hasClass("autoClose")) {
                window.setTimeout(function () {
                    sideNav.enableAutoClose = true;
                }, 5);
            }
        };
        button.click(openSideNav);
        sideNav.find("> li.closeSideNav > a").click(closeSideNav);
        if (sideNav.hasClass("autoClose")) {
            sideNav.find("a:not(.disableAutoClose)").click(function () {
                closeSideNav();
            });
            $(document).click(function (event) {
                if (sideNav.enableAutoClose && (!(sideNav.is(event.target) || (sideNav.find(event.target).length > 0)))) {
                    closeSideNav();
                }
            });
        }
    });
}